module.exports = function (grunt) {
	grunt.initConfig({

		DEST: '../../../../build/craft/plugins/events',
		SRC: 'events',

		uglify: {
			options: {
				mangle: false
			},
			dependencies: {
				files: {
					'<%= SRC %>/resources/dist/js/dependencies-public.min.js': [
						'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
						'bower_components/imagesloaded/imagesloaded.pkgd.min.js',
						'bower_components/jquery-masonry/dist/masonry.pkgd.min.js'
					]
				}
			},
			build: {
				files: {
					'<%= SRC %>/resources/dist/js/scripts-cp.min.js': '<%= SRC %>/resources/js/scripts-cp.js',
					'<%= SRC %>/resources/dist/js/scripts-public.min.js': '<%= SRC %>/resources/js/scripts-public.js'
				}
			}
		},

		watchFiles: {
			css: {
				files: [
					'<%= SRC %>/resources/less/**/*.less'
				],
				tasks: 'less'
			},
			php: {
				files: [
					'<%= SRC %>/**/*.php'
				],
				tasks: 'copy:php'
			},
			html: {
				files: [
					'<%= SRC %>/**/*.html'
				],
				tasks: 'copy:html'
			},
			js: {
				files: [
					'<%= SRC %>/resources/js/**/*.js'
				],
				tasks: 'uglify:build'
			},
			distFiles: {
				files: [
					'<%= SRC %>/resources/dist/**/*'
				],
				tasks: 'copy:distFiles'
			}
		},

		less: {
			build: {
				options: {
					compress: true,
					plugins: [
						new (require('less-plugin-autoprefix'))({browsers: ['last 5 versions']})
					]
				},
				files: {
					'<%= SRC %>/resources/dist/css/styles-cp.min.css': '<%= SRC %>/resources/less/styles-cp.less',
					'<%= SRC %>/resources/dist/css/styles-public.min.css': '<%= SRC' +
					' %>/resources/less/styles-public.less'
				}
			}
		},

		copy: {
			build: {
				files: [
					{
						expand: true,
						dest: '<%= DEST %>',
						cwd: '<%= SRC %>/',
						src: [
							'**/*',
							'!resources/js/**',
							'!resources/less/**'
						]
					}
				]
			},
			php: {
				files: [
					{
						expand: true,
						dest: '<%= DEST %>',
						cwd: '<%= SRC %>/',
						src: [
							'**/*.php'
						]
					}
				]
			},
			html: {
				files: [
					{
						expand: true,
						dest: '<%= DEST %>',
						cwd: '<%= SRC %>/',
						src: [
							'**/*.html'
						]
					}
				]
			},
			distFiles: {
				files: [
					{
						expand: true,
						dest: '<%= DEST %>',
						cwd: '<%= SRC %>/',
						src: [
							'resources/dist/**/*'
						]
					}
				]
			}
		}
	});

	// Load tasks
	require('load-grunt-tasks')(grunt);

	// Rename tasks so we can use the name instead, because they are short and sweet
	grunt.renameTask('watch', 'watchFiles');

	// Public tasks
	grunt.registerTask('watch', [
		'build-assets',
		'watchFiles'
	]);

	// Private tasks
	grunt.registerTask('build-assets', [
		'less',
		'copy:build',
		'uglify:build',
		'uglify:dependencies'
	]);
};