# Craft Events

This Craft plugin is based on the Pixel and Tonic [Events plugin](https://github.com/pixelandtonic/Events).

## Installation

1. Upload the `events` directory to `craft/plugins/` on your server.
2. Enable the plugin under Craft Admin > Settings > Plugins

## Setup

Create a calendar through the cp and make sure to properly configure the event url format and template. In your calendar html template, use this plugin variable.

```
{{ craft.events.displayCalendar({
    'calendarId': calendarId
}) }}
```

Use this plugin variable for displaying the event detail information in your event template.

```
{{ craft.events.displayEvent(event) }}
```

## Examples

### Widget

This example get's the upcoming events.

```
{% set calendar = craft.events.getCalendarById(calendarId) %}

{% set events = craft.events.getEvents({
    'calendarId': calendar.id,
    'endDate': [
        'and',
        '>= ' ~ now
    ],
    'order': 'startDate ASC',
    'limit': 3
}) %}

<ul>

{% for event in events %}
    <li>
    
        <h3 class="media-heading">
            {{ craft.events.renderMacro({
                macro: 'eventTitle',
                variables: {
                    event: event,
                    calendar: calendar
                }
            }) }}
        </h3>
    
        <span class="meta">
            {{ craft.events.renderMacro({
                macro: 'eventDateMeta',
                variables: {
                    event: event
                }
            }) }}
        </span>
    
        {{ craft.events.renderMacro({
            macro: 'eventExcerpt',
            variables: {
                event: event,
                calendar: calendar
            }
        }) }}
    
    </li>
{% endfor %}

</ul>
```