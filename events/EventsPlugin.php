<?php
namespace Craft;

/**
 * Events plugin class
 */
class EventsPlugin extends BasePlugin
{
    public function getName()
    {
        return 'Events';
    }

    public function getDescription()
    {
        return 'An awesome events plugin.';
    }

    public function getDocumentationUrl()
    {
        return 'https://bitbucket.org/sproing-team/craft-events';
    }

    public function getVersion()
    {
        return '1.0.2';
    }

    public function getReleaseFeedUrl()
    {
        return 'https://bitbucket.org/sproing-team/craft-events/raw/master/events/releases.json';
    }

    public function getDeveloper()
    {
        return 'Sproing Creative';
    }

    public function getDeveloperUrl()
    {
        return 'http://sproing.ca';
    }

    public function hasCpSection()
    {
        return true;
    }

    public function registerCpRoutes()
    {
        return [
            'events/calendars' => ['action' => 'events/calendars/calendarIndex'],
            'events/calendars/new' => ['action' => 'events/calendars/editCalendar'],
            'events/calendars/(?P<calendarId>\d+)' => ['action' => 'events/calendars/editCalendar'],
            'events' => ['action' => 'events/eventIndex'],
            'events/(?P<calendarHandle>{handle})/new' => ['action' => 'events/editEvent'],
            'events/(?P<calendarHandle>{handle})/(?P<eventId>\d+)' => ['action' => 'events/editEvent'],
        ];
    }

    public function registerSiteRoutes()
    {
        // Register public routes of all calendars
        $calendars = craft()->events_calendars->getAllCalendars();
        $routes = [];

        foreach ($calendars as $calendar) {
            $routes[$calendar['urlFormat'] . '/(?P<eventId>\d+)'] = ['action' => 'events/eventIndexPublic'];
        }

        return $routes;
    }
}
