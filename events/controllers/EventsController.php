<?php
namespace Craft;

/**
 * Events controller
 */
class EventsController extends BaseController
{
    protected $allowAnonymous = true;

    /**
     * Event index
     */
    public function actionEventIndex()
    {
        $variables['calendars'] = craft()->events_calendars->getAllCalendars();

        $this->renderTemplate('events/_index', $variables);
    }

    /**
     * Public Event index
     * @param array $variables
     */
    public function actionEventIndexPublic(array $variables = [])
    {
        $variables['event'] = craft()->events->getEventById($variables['eventId']);
        $variables['calendar'] = craft()->events_calendars->getCalendarById($variables['event']->calendarId);

        $oldPath = craft()->path->getTemplatesPath();

        // Check for override templates
        $mainPath = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($mainPath);

        $this->renderTemplate($variables['calendar']->template, $variables);

        // Reset default template path
        craft()->path->setTemplatesPath($oldPath);
    }

    /**
     * Edit an event.
     *
     * @param array $variables
     * @throws HttpException
     */
    public function actionEditEvent(array $variables = [])
    {
        if (!empty($variables['calendarHandle'])) {
            $variables['calendar'] = craft()->events_calendars->getCalendarByHandle($variables['calendarHandle']);
        } else if (!empty($variables['calendarId'])) {
            $variables['calendar'] = craft()->events_calendars->getCalendarById($variables['calendarId']);
        }

        if (empty($variables['calendar'])) {
            throw new HttpException(404);
        }

        // Now let's set up the actual event
        if (empty($variables['event'])) {
            if (!empty($variables['eventId'])) {
                $variables['event'] = craft()->events->getEventById($variables['eventId']);

                if (!$variables['event']) {
                    throw new HttpException(404);
                }
            } else {
                $variables['event'] = new Events_EventModel();
                $variables['event']->calendarId = $variables['calendar']->id;
            }
        }

        if (!$variables['event']->id) {
            $variables['title'] = Craft::t('Create a new event');
            $variables['featuredImageId'] = '';
        } else {
            $variables['title'] = $variables['event']->title;
            $variables['featuredImageId'] = $variables['event']->featuredImageId;
        }

        // Tabs
        $variables['tabs'] = [];

        foreach ($variables['calendar']->getFieldLayout()->getTabs() as $index => $tab) {
            // Do any of the fields on this tab have errors?
            $hasErrors = false;

            if ($variables['event']->hasErrors()) {
                foreach ($tab->getFields() as $field) {
                    if ($variables['event']->getErrors($field->getField()->handle)) {
                        $hasErrors = true;
                        break;
                    }
                }
            }

            $variables['tabs'] = [
                [
                    'label' => 'Details',
                    'url' => '#tab' . ($index),
                    'class' => ($hasErrors ? 'error' : null)
                ],
                [
                    'label' => $tab->name,
                    'url' => '#tab' . ($index + 1),
                    'class' => ($hasErrors ? 'error' : null)
                ]
            ];
        }

        // Breadcrumbs
        $variables['crumbs'] = [
            [
                'label' => Craft::t('Events'),
                'url' => UrlHelper::getUrl('events')
            ],
            [
                'label' => $variables['calendar']->name,
                'url' => UrlHelper::getUrl('events')
            ]
        ];

        $variables['viewUrl'] = craft()->events->getEventLink($variables['event'], $variables['calendar']);

        // Set the "Continue Editing" URL
        $variables['continueEditingUrl'] = 'events/' . $variables['calendar']->handle . '/{id}';

        // Set thumbnail - asset elements
        if ($variables['featuredImageId']) {
            $asset = craft()->elements->getElementById($variables['featuredImageId']);
            $variables['featuredImage_element'] = [$asset];
        } else {
            $variables['featuredImage_element'] = [];
        }

        $variables['featuredImage_elementType'] = craft()->elements->getElementType(ElementType::Asset);

        // Render the template!
        $this->renderTemplate('events/_edit', $variables);
    }

    /**
     * Saves an event.
     */
    public function actionSaveEvent()
    {
        $this->requirePostRequest();

        $eventId = craft()->request->getPost('eventId');

        if ($eventId) {
            $event = craft()->events->getEventById($eventId);

            if (!$event) {
                throw new Exception(Craft::t('No event exists with the ID “{id}”', ['id' => $eventId]));
            }
        } else {
            $event = new Events_EventModel();
        }

        // Set the event attributes, defaulting to the existing values for whatever is missing from the post data
        $event->calendarId = craft()->request->getPost('calendarId', $event->calendarId);
        $event->startDate = (($startDate = craft()->request->getPost('startDate')) ? DateTime::createFromString($startDate, craft()->timezone) : null);
        $event->endDate = (($endDate = craft()->request->getPost('endDate')) ? DateTime::createFromString($endDate, craft()->timezone) : null);
        $event->description = craft()->request->getPost('description', $event->description);
        $event->excerpt = craft()->request->getPost('excerpt', $event->excerpt);
        $event->ticketsUrl = craft()->request->getPost('ticketsUrl', $event->ticketsUrl);
        $event->venueName = craft()->request->getPost('venueName', $event->venueName);
        $event->address = craft()->request->getPost('address', $event->address);

        $featuredImageId = craft()->request->getPost('featuredImageId', $event->featuredImageId);
        $event->featuredImageId = (!empty($featuredImageId) ? $featuredImageId[0] : null);

        $event->getContent()->title = craft()->request->getPost('title', $event->title);
        $event->setContentFromPost('fields');


        if (craft()->events->saveEvent($event)) {
            craft()->userSession->setNotice(Craft::t('Event saved.'));
            $this->redirectToPostedUrl($event);
        } else {
            craft()->userSession->setError(Craft::t('Couldn’t save event.'));

            // Send the event back to the template
            craft()->urlManager->setRouteVariables(['event' => $event]);
        }
    }

    /**
     * Deletes an event.
     */
    public function actionDeleteEvent()
    {
        $this->requirePostRequest();

        $eventId = craft()->request->getRequiredPost('eventId');

        if (craft()->elements->deleteElementById($eventId)) {
            craft()->userSession->setNotice(Craft::t('Event deleted.'));
            $this->redirectToPostedUrl();
        } else {
            craft()->userSession->setError(Craft::t('Couldn’t delete event.'));
        }
    }
}
