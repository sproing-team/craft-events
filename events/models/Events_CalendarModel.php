<?php
namespace Craft;

/**
 * Events - Calendar model
 */
class Events_CalendarModel extends BaseModel
{
    public $urlFormatIsRequired = false;

    public function rules()
    {
        $rules = parent::rules();

        if ($this->urlFormatIsRequired) {
            $rules[] = [
                'urlFormat',
                'required'
            ];
        }

        return $rules;
    }

    /**
     * Use the translated calendar name as the string representation.
     *
     * @return string
     */
    function __toString()
    {
        return Craft::t($this->name);
    }

    /**
     * @access protected
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'id' => AttributeType::Number,
            'name' => AttributeType::String,
            'handle' => AttributeType::String,
            'fieldLayoutId' => AttributeType::Number,
            'template' => AttributeType::String,
            'urlFormat' => [
                AttributeType::UrlFormat,
                'label' => 'URL Format'
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'fieldLayout' => new FieldLayoutBehavior('Events_Event'),
        ];
    }
}
