<?php
namespace Craft;

/**
 * Events - Calendar record
 */
class Events_CalendarRecord extends BaseRecord
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'events_calendars';
    }

    /**
     * @access protected
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'name' => [
                AttributeType::Name,
                'required' => true
            ],
            'handle' => [
                AttributeType::Handle,
                'required' => true
            ],
            'fieldLayoutId' => AttributeType::Number,
            'urlFormat' => [
                AttributeType::UrlFormat,
                'required' => true
            ],
            'template' => [
                AttributeType::String,
                'required' => true
            ],
        ];
    }

    /**
     * @return array
     */
    public function defineRelations()
    {
        return [
            'fieldLayout' => [
                static::BELONGS_TO,
                'FieldLayoutRecord',
                'onDelete' => static::SET_NULL
            ],
            'events' => [
                static::HAS_MANY,
                'Events_EventRecord',
                'eventId'
            ],
        ];
    }

    /**
     * @return array
     */
    public function defineIndexes()
    {
        return [
            [
                'columns' => ['name'],
                'unique' => true
            ],
            [
                'columns' => ['handle'],
                'unique' => true
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'ordered' => ['order' => 'name'],
        ];
    }
}
