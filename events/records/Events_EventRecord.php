<?php
namespace Craft;

/**
 * Events - Event record
 */
class Events_EventRecord extends BaseRecord
{
    /**
     * @return string
     */
    public function getTableName()
    {
        return 'events';
    }

    /**
     * @access protected
     * @return array
     */
    protected function defineAttributes()
    {
        return [
            'startDate' => [
                AttributeType::DateTime,
                'required' => true
            ],
            'endDate' => [
                AttributeType::DateTime,
                'required' => true
            ],
            'description' => [
                AttributeType::Mixed,
                'required' => false
            ],
            'excerpt' => [
                AttributeType::Mixed,
                'required' => false
            ],
            'ticketsUrl' => [
                AttributeType::Url,
                'required' => false
            ],
            'venueName' => [
                AttributeType::String,
                'required' => false
            ],
            'address' => [
                AttributeType::Mixed,
                'required' => false
            ]
        ];
    }

    /**
     * @return array
     */
    public function defineRelations()
    {
        return [
            'element' => [
                static::BELONGS_TO,
                'ElementRecord',
                'id',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'calendar' => [
                static::BELONGS_TO,
                'Events_CalendarRecord',
                'required' => true,
                'onDelete' => static::CASCADE
            ],
            'featuredImage' => [
                static::BELONGS_TO,
                'AssetFileRecord'
            ],
        ];
    }
}
