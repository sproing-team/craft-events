(function () {
	// Date picker
	var datePicker = $('#startDate').datetimepicker({
		format: 'MMMM YYYY',
		showTodayButton: false,
		icons: {
			previous: 'glyphicon glyphicon-arrow-left',
			next: 'glyphicon glyphicon-arrow-right'
		}
	});

	// Update date on new date selection
	datePicker.on('dp.change', function (e) {
		fetchEvents({
			date: e.date
		});
	});

	// Toggle next and previous months
	$('#nextMonth').on('click', function () {
		var newDate = datePicker.data("DateTimePicker").date();
		fetchEvents({
			date: newDate.add(1, 'month')
		});
	});
	$('#prevMonth').on('click', function () {
		var newDate = datePicker.data("DateTimePicker").date();
		fetchEvents({
			date: newDate.subtract(1, 'month')
		});
	});

	// Change layout
	$('.calendar-filter-options .btn-group .btn').on('click', function () {
		fetchEvents({
			display: $(this).attr('rel')
		});
	});

	// Masonry
	var $grid = $('.posterboard').imagesLoaded(function () {
		$grid.masonry({
			itemSelector: '.col-md-4',
			percentPosition: true
		});
	});
	$grid.one('layoutComplete', function () {
		$('.loading-view').hide();
		$('.loading-wrap').addClass('masonry-loaded');
	});

	// Build get parameters
	var fetchEvents = function (e) {
		var date = $('#startDate').data("DateTimePicker").date(),
			catID = $('#eventCategory').val(),
			display = $('.btn-group .btn.active').attr('rel'),
			params = {};

		if (e.date) {
			date = e.date;
		}
		params.date = moment(date).format("YYYY-MM-DD");

		if (e.cat) {
			catID = e.cat;
		}

		if (catID != 'all' && catID != null) {
			params.cat = catID;
		}

		if (e.display) {
			params.display = e.display;
		} else {
			params.display = display;
		}

		window.location.href = '?' + jQuery.param(params);
	}
})();