<?php
namespace Craft;

/**
 * Events service
 */
class EventsService extends BaseApplicationComponent
{
    public function getEvents($options)
    {
        // If fetching extra events, use limit total
        $maxTotalFutureEvents = 12;

        $criteria = craft()->elements->getCriteria('Events_Event');

        foreach ($options as $key => $value) {
            $criteria->$key = $value;
        }

        $results = craft()->elements->findElements($criteria);
        $resultsFound = count($results);

        // Grab more future events if there are less found than the limit amount
        if ($resultsFound && $resultsFound <= $maxTotalFutureEvents) {
            $results = $this->_getAdditionalFutureEvents($results, $resultsFound, $options);
        }

        return $results;
    }

    private function _getAdditionalFutureEvents($results, $resultsFound, $options)
    {
        $criteria = craft()->elements->getCriteria('Events_Event');

        $lastFoundEvent = end($results);
        $lastEventStartDate = $lastFoundEvent['startDate'];

        $criteria->calendarID = $options['calendarId'];
        $criteria->offset = $resultsFound;
        $criteria->startDate = [
            'and',
            '>= ' . $lastEventStartDate
        ];

        $resultsAdditional = craft()->elements->findElements($criteria);

        $results = array_merge($results, $resultsAdditional);

        return $results;
    }

    public function getEventLink($event, $calendar)
    {
        $url = craft()->getSiteUrl() . rtrim($calendar->urlFormat) . '/' . $event->id;

        return $url;
    }

    /**
     * Returns an event by its ID.
     *
     * @param int $eventId
     * @return Events_EventModel|null
     */
    public function getEventById($eventId)
    {
        return craft()->elements->getElementById($eventId, 'Events_Event');
    }

    public function displayEvent($event)
    {
        $variables['event'] = $event;
        $variables['calendar'] = craft()->events_calendars->getCalendarById($event->calendarId);

        $oldPath = craft()->path->getTemplatesPath();

        // Check for override templates
        $mainPath = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($mainPath);

        if (craft()->templates->doesTemplateExist('events/_entry')) {
            // Found override template
            $html = craft()->templates->render('events/_entry', $variables);
        } else {
            // Use template located in plugin dir
            $newPath = craft()->path->getPluginsPath() . 'events/templates';
            craft()->path->setTemplatesPath($newPath);

            $html = craft()->templates->render('public/_entry', $variables);
        }

        // Include resources
        $html .= craft()->templates->includeCssResource('events/dist/css/styles-public.min.css');
        $html .= craft()->templates->includeJsResource('events/dist/js/dependencies-public.min.js');
        $html .= craft()->templates->includeJsResource('events/dist/js/scripts-public.min.js');

        // Reset default template path
        craft()->path->setTemplatesPath($oldPath);

        return TemplateHelper::getRaw($html);
    }

    public function renderMacro($options)
    {
        $oldPath = craft()->path->getTemplatesPath();

        // Use template located in plugin dir
        $newPath = craft()->path->getPluginsPath() . 'events/templates';
        craft()->path->setTemplatesPath($newPath);


        $html = craft()->templates->renderMacro('public/_macros', $options['macro'], $options['variables']);

        // Reset default template path
        craft()->path->setTemplatesPath($oldPath);

        return TemplateHelper::getRaw($html);
    }

    public function saveEvent(Events_EventModel $event)
    {
        $isNewEvent = !$event->id;

        // Event data
        if (!$isNewEvent) {
            $eventRecord = Events_EventRecord::model()->findById($event->id);

            if (!$eventRecord) {
                throw new Exception(Craft::t('No event exists with the ID “{id}”', ['id' => $event->id]));
            }
        } else {
            $eventRecord = new Events_EventRecord();
        }

        $eventRecord->calendarId = $event->calendarId;
        $eventRecord->startDate = $event->startDate;
        $eventRecord->endDate = $event->endDate;
        $eventRecord->description = $event->description;
        $eventRecord->excerpt = $event->excerpt;
        $eventRecord->featuredImageId = $event->featuredImageId;
        $eventRecord->ticketsUrl = $event->ticketsUrl;
        $eventRecord->venueName = $event->venueName;
        $eventRecord->address = $event->address;

        $eventRecord->validate();
        $event->addErrors($eventRecord->getErrors());

        if (!$event->hasErrors()) {
            $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;
            try {
                // Fire an 'onBeforeSaveEvent' event
                $this->onBeforeSaveEvent(new Event($this, [
                    'event' => $event,
                    'isNewEvent' => $isNewEvent
                ]));

                if (craft()->elements->saveElement($event)) {
                    // Now that we have an element ID, save it on the other stuff
                    if ($isNewEvent) {
                        $eventRecord->id = $event->id;
                    }

                    $eventRecord->save(false);

                    // Fire an 'onSaveEvent' event
                    $this->onSaveEvent(new Event($this, [
                        'event' => $event,
                        'isNewEvent' => $isNewEvent
                    ]));

                    if ($transaction !== null) {
                        $transaction->commit();
                    }

                    return true;
                }
            } catch (\Exception $e) {
                if ($transaction !== null) {
                    $transaction->rollback();
                }

                throw $e;
            }
        }

        return false;
    }

    // Variables
    public function getAsset($assetId)
    {
        $asset = craft()->assets->getFileById($assetId);

        return $asset;
    }

    // Events
    public function onBeforeSaveEvent(Event $event)
    {
        $this->raiseEvent('onBeforeSaveEvent', $event);
    }

    public function onSaveEvent(Event $event)
    {
        $this->raiseEvent('onSaveEvent', $event);
    }
}
