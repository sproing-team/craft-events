<?php
namespace Craft;

class EventsVariable
{
    function getEvents($options = [])
    {
        return craft()->events->getEvents($options);
    }

    function getEventById($eventId)
    {
        return craft()->events->getEventById($eventId);
    }

    function getCalendarById($calendarId)
    {
        return craft()->events_calendars->getCalendarById($calendarId);
    }

    function displayCalendar($options = [])
    {
        return craft()->events_calendars->displayCalendar($options);
    }

    // Access plugin macros in main templates
    function renderMacro($options = [])
    {
        return craft()->events->renderMacro($options);
    }

    function displayEvent($event)
    {
        return craft()->events->displayEvent($event);
    }

    // Used for getting featured image related to an event
    public function getAsset($assetId)
    {
        return craft()->events->getAsset($assetId);
    }
}